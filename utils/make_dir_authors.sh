#!/bin/bash


################################################################################
# 定数・変数
################################################################################

export LANG=ja_JP.UTF-8

pushd "$(dirname $0)" > /dev/null
SCRIPTPATH="$(pwd)"
popd > /dev/null

REPOSITORY_ROOT="$(cygpath "$(git rev-parse --show-toplevel)")"

OUTFILE=AUTHORS.txt
TMPFILE=AUTHORS.tmp
MAPFILE=${SCRIPTPATH}/name_map.csv


# 名前マッピング用の連想配列(名前 -> マップ後の名前)
declare -A name_map

################################################################################
# 関数
################################################################################


# 名前マッピング用の連想配列を初期化
function InitNameMap() {

	while IFS=, read f1 f2
	do
		if [ "$f1" = "" ]; then
			continue
		fi
		if [ "$f2" = "" ]; then
			continue
		fi 
		
		name_map["${f1}"]="${f2}"
		
	done < <(perl -pe 's/^\xEF\xBB\xBF//g; s/\r//' "${MAPFILE}")
}

# 標準入力をマップ後の名前に変換して標準出力に出力する
function MapNames() {

	if [ ${#name_map[@]} -eq 0 ]; then
		InitNameMap
	fi
	
	while read original_name; do
		if [ -v name_map["${original_name}"] ]; then
			echo "${name_map["${original_name}"]}"
		else
			echo "${original_name}"
		fi
	done 
	
	return 0
}

# Gitログから貢献者のリストを取得する
function GetAuthorNamesFromGitLog() {
	local IFS=$'\n'
	
	local names=($( \
		git --no-pager log  --reverse --pretty=format:"%an" . \
			| grep -v -e 'www-data' -e 'system' -e 'guest'  \
			| MapNames \
			| awk '!x[$0]++' \
	))
	
	printf '%s\n' "${names[@]}" 
	return 0
}


# サブディレクトリのAUTHORS.txtから貢献者のリストを取得する
function GetAuthorNamesFromSubDir() {

	local IFS=$'\n'
	local names=($( \
		find . -mindepth 2 -iname 'AUTHORS.txt' -print0 \
			| xargs -0 sed -E 's/$/\n/g' \
			| perl -pe 's/^\xEF\xBB\xBF//g; s/\r//; s/\t.*$//g' \
			| sed -E '/^#/d' \
			| MapNames \
			| awk '!x[$0]++' \
	))

	printf '%s\n' "${names[@]}" 
	return 0
}

################################################################################
# メイン
################################################################################

if [ "${REPOSITORY_ROOT}" = "" ]; then
	echo "Gitリポジトリ内ではありません。" >&2
	exit 1
fi


IFS=$'\n'
names_all=()
names_all+=($( \
	GetAuthorNamesFromGitLog \
))
names_all+=($( \
	GetAuthorNamesFromSubDir \
))

echo -n "" > "${TMPFILE}"
if [ "$(pwd)" = "${REPOSITORY_ROOT}" ]; then
	echo "################################################################################" >> "${TMPFILE}"
	echo "# This file is contributors list of this repository. "                            >> "${TMPFILE}"
	echo "#-------------------------------------------------------------------------------" >> "${TMPFILE}"
	echo "# このファイルは、このリポジトリの貢献者リストです。 "                            >> "${TMPFILE}"
	echo "################################################################################" >> "${TMPFILE}"
else
	echo "################################################################################" >> "${TMPFILE}"
	echo "# This file is contributors list of this directory and its sub directories. "     >> "${TMPFILE}"
	echo "# See <repository root>/common/AUHTORS.txt for contributors of common materials." >> "${TMPFILE}"
	echo "#-------------------------------------------------------------------------------" >> "${TMPFILE}"
	echo "# このファイルはこのディレクトリおよびサブディレクトリの貢献者リストです。"       >> "${TMPFILE}"
	echo "# 共通資料の貢献者は<リポジトリルート>/common/AUTHORS.txtをご覧ください。 "       >> "${TMPFILE}"
	echo "################################################################################" >> "${TMPFILE}"

fi

echo "#"  >> "${TMPFILE}"
echo "# Note: "                                                               >> "${TMPFILE}"
echo "#   - Some parts of this file are auto-generated. "                     >> "${TMPFILE}"
echo "#   - This file contains Unicode characters and is encoded in UTF-8."   >> "${TMPFILE}"
echo "#   - Acknowledgements are not included."                               >> "${TMPFILE}"
echo "#   - Authors of external material(s) may not be included."             >> "${TMPFILE}"
echo "#     Their names may be available at its file(s) or document(s)."      >> "${TMPFILE}"
echo "#   - Auto-generated part of this contributors list is sorted by first" >> "${TMPFILE}"
echo "#     date in ascending order. If contribution date is unknown, their " >> "${TMPFILE}"
echo "#     names are listed in the last. "                                   >> "${TMPFILE}"
echo "#   - Revision history is available at "                                >> "${TMPFILE}"
echo "#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep02 . "           >> "${TMPFILE}"
echo "#"  >> "${TMPFILE}"
echo "# 注記: "                                                                          >> "${TMPFILE}"
echo "#   - このファイルの一部は自動生成されたものです。"                                >> "${TMPFILE}"
echo "#   - このファイルにはUnicode文字が含まれており、UTF-8でエンコードされています。"  >> "${TMPFILE}"
echo "#   - 謝辞は含まれていません。"                                                    >> "${TMPFILE}"
echo "#   - 外部の素材や資料の著者情報は含まれていないことがあります。それらの著者情報"  >> "${TMPFILE}"
echo "#     は当該の素材や資料のファイルやドキュメントに記載されていることがあります。"  >> "${TMPFILE}"
echo "#   - この貢献者リストの自動生成部分は、最初の貢献日の昇順に並べられています。"    >> "${TMPFILE}"
echo "#     もし、貢献日が不明の場合はリストの最後に記載されています。"                  >> "${TMPFILE}"
echo "#   - 更新履歴は以下からご覧いただけます。 "                                       >> "${TMPFILE}"
echo "#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep02  "                       >> "${TMPFILE}"
echo "#"  >> "${TMPFILE}"


echo "" >> "${TMPFILE}"
	
printf '%s\n' "${names_all[@]}" \
	| awk '!x[$0]++' \
	>> "${TMPFILE}"


echo "### END AUTO-GENERATED LIST.  LEAVE THIS LINE HERE; SCRIPTS LOOK FOR IT. ###" >> "${TMPFILE}"

if [ -f "${OUTFILE}" ]; then
	grep '^### END AUTO-GENERATED LIST\.' "${OUTFILE}" > /dev/null
	if [ $? -eq 0 ]; then
		sed -e '1,/^### END AUTO-GENERATED LIST\./ d' "${OUTFILE}" | perl -pe 's/^\xEF\xBB\xBF//g; s/\r//' >>  "${TMPFILE}"
	else
		 perl -pe 's/^\xEF\xBB\xBF//g; s/\r//' "${OUTFILE}" >>  "${TMPFILE}"
	fi
fi

perl -pe 's/\n/\r\n/' "${TMPFILE}" > "${OUTFILE}"
rm -f "${TMPFILE}" 

