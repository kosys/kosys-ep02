#!/bin/bash

export LANG=ja_JP.UTF-8

pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd`
popd > /dev/null


cd "${SCRIPTPATH}/../"

ffmpeg="${SCRIPTPATH}/bin/ffmpeg.exe"
doga_output="${SCRIPTPATH}/../doga/_output"
outfile="${SCRIPTPATH}/../_release/authors.html"
imgdir="${SCRIPTPATH}/../_release/img"



function GetAuthors() {
	
	local IFS=$'\n'
	local names=()
	declare -A name2notes
	
	while IFS="	" read f1 f2
	do
		if [ "${f1}" = "" ]; then
			continue
		fi
		names+=("${f1}")
		if [ ! -v name2notes["${f1}"] ]; then
			name2notes["${f1}"]=""
		fi
		
		name2notes["${f1}"]="${name2notes["${f1}"]},${f2}"
	done < <(
		perl -pe 's/^\xEF\xBB\xBF//g; s/\r//' AUTHORS.txt \
			| sed '/^#/d' \
			| awk '!x[$0]++' \
	)
	
	printf '%s\n' "${names[@]}" \
		| awk '!x[$0]++' \
		| while IFS="	" read -r name
	do
		if [ "${name}" = "" ]; then
			continue
		fi
		
		local note="$( \
			echo -n "${name2notes["$name"]}" \
				| sed 's/,/\n/g' \
				| sort \
				| uniq \
				| grep -v '^\s*$' \
				| perl -pe 's/\n/,/g' \
		)"
				
		echo "${name}	${note}"

	done
	
}




mkdir "${imgdir}"

cat <<EOM > "${outfile}"
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>こうしす！第2話 カット別貢献者一覧表（非公式）</title>
	<style>
	
		.authorlist
		{
			width: 100%;
			border: solid 1px black;
		}
	
		.authorlist>tbody>tr>* {
			border: solid 1px black;
		}
		
		.author {
			width: 50%;
		}
	
		.note {
			width: auto;
		}
		
		.authorlist>tbody>tr>th {
			text-align: left;
			font-weight: normal;
			background-color: #f5deb3;
		}
		
		th.author {
			width: 200px;
		}
	
		th.note {
			width: auto;
		}
		
		.warn {
			font-weight: bold;
			color: red;
		}
	</style>

</head>
<body>

<h1>こうしす！第2話 カット別貢献者一覧表（非公式）</h1>

<h2>はじめに</h2>

<ul>
<li span class="warn">この一覧は非公式に作成されたものです。あくまでも参考としてご活用ください。</li>
<li>正式なクレジットは<a href="https://opap.jp/wiki/redirect/kosys_ep02_authors">https://opap.jp/wiki/redirect/kosys_ep02_authors</a>をご覧下さい。</li>
<li>この一覧はカット別の貢献者を、更新履歴から投稿日順に並び替えて作成したものです。投稿日が不明な場合や手入力によって追加された貢献者リストは、各リストの最後に出力されています。</li>
<li>カットによって、共通素材・資料の作成者（キャラデザ）のクレジットが入っていたり入っていなかったりしますが仕様です。</li>
<li>備考欄の記載は手作業によるもので正確さに欠きます。</li>
<li>この一覧には、作画に影響しない細かい変更履歴（ファイルの移動等）も含まれます。各貢献者がどのような役割で貢献したかについては、<a href="https://opap.jp/wiki/redirect/kosys_ep02_authors">正式なクレジット</a>をご覧下さい。</li>
</ul>

<h2>カット別貢献者一覧</h2>

EOM

for line in $(find doga -mindepth 2 -maxdepth 2 -type d); do 
	echo "${line}"
	pushd "${line}" > /dev/null
	
	scene_num=$(basename $(dirname "${line}"))
	cut_num=$(basename "${line}")
	
	echo "<h3>${scene_num}_${cut_num}</h3>" >>"${outfile}"
	
	videofile="${doga_output}/${scene_num}_${cut_num}.avi"
	imgfile="${imgdir}/${scene_num}_${cut_num}.jpg"
	if [ -f "${videofile}" ]; then
	    
	    if [ "${videofile}" -nt "${imgfile}" ]; then
	        rm -f "${imgfile}"
	    fi
	    
	    if [ ! -f "${imgfile}" ]; then
	        "${ffmpeg}" -i "${videofile}" -vframes 1 -ss     4.0 -f image2 -s 640x360 -y "${imgfile}"
	    fi
	    
	    if [ ! -f "${imgfile}" ]; then
	        "${ffmpeg}" -i "${videofile}" -vframes 1 -sseof -0.1 -f image2 -s 640x360 -y "${imgfile}"
	    fi
	    
		echo "<img class='thumb' src='img/${scene_num}_${cut_num}.jpg' />" >>"${outfile}"
	fi
	
	echo "<table class='authorlist'>" >>"${outfile}"
	echo "<tr><th class='author'>名前</th><th class='note'>備考</th>" >>"${outfile}"
	
	GetAuthors \
		| while IFS="	" read f1 f2
	do
		if [ "${f1}" = "" ]; then
			continue
		fi
		f2=$(echo -n "${f2}" | sed 's/,/, /g')
		echo "<tr><td class='author'>${f1}</td><td class='note'>${f2}</td></tr>" >>"${outfile}"
	done
	echo "</table>" >>"${outfile}"
	
	popd > /dev/null
done


echo "</body></html>" >> "${outfile}"
