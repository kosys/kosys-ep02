#!/bin/bash

export LANG=ja_JP.UTF-8
export LC_ALL=C

pushd `dirname "$0"` > /dev/null
SCRIPTPATH=`pwd`
popd > /dev/null

cd "${SCRIPTPATH}/../"

find . -type d  \( -path ./.git -o -path ./utils/bin -o -path ./utils/src -o -path ./common -o -path ./_release -o -path ./doga/_output \) -prune -o -type d -print \
	| tac \
	| \
while IFS=$'\n' read -r line; do
	echo "$line"
	pushd "${line}" > /dev/null
	
	git ls-files --error-unmatch &> /dev/null
	
	if [ $? -eq 0 ]; then
		"${SCRIPTPATH}/make_dir_authors.sh"
		git add AUTHORS.txt
	else
		echo "Skipped."
	fi
	
	popd > /dev/null
done

