#!/bin/bash

trap '
	exitcode=$?
	kill $(jobs -p) &> /dev/null || true;
	trap - EXIT; 
	exit $exitcode
' EXIT

CMD="$1"

shift

if [ "$LANG" == "" ]; then
    # MSYS環境を想定
    "${CMD}" "$@"
else
    # MSYS2 & mtty 環境を想定
    "${CMD}" "$@" 2>&1 | iconv -f cp932
fi


exit ${PIPESTATUS[0]}
