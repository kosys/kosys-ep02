こうしす！ #2 
=============

**「やはり弊社の業務システムはまちがっている」**

情報セキュリティ普及啓発アニメ「こうしす！」シリーズ第2弾。

## 概要
これは「こうしす！#2」の制作に使用した素材や撮影用のプロジェクトファイルなどをまとめたものです。
完成動画は動画共有サイトにアップロードされています。完成作品のみが必要な方は以下をご覧ください。

* [ニコニコ](http://ch.nicovideo.jp/kosys)
* [YouTube](https://www.youtube.com/user/OPAPJP)

また、ダウンロード用の動画ファイルは以下から入手可能です。(2016年7月現在）

[https://osdn.jp/projects/kosys/releases/](https://osdn.jp/projects/kosys/releases/)


## 作品内容（あらすじ）
XP事件以降、システム課の通常業務から外され、研修名目で現場を転々としていたアカネだったが、ついに子会社への出向を命じられてしまう。そこで待ち受けていたシステム開発案件とは――

なぜ脆弱な情報システムが生まれてしまうのか？　システム開発現場の謎に迫る！



## レンダリングについて
### 要求環境
1. ハードウェアおよびOS
   * amd64命令をサポートするCPU
   * CUDAが使用できるCPU
   * メモリ 8GB以上（32GB以上推奨）
   * HDD 130GB以上の空き領域　（一時ファイルの出力先はSSD上に設定することを推奨）
   * Windows 10 64bit
2. 以下のソフトウェアがインストールされていること
   * Adobe After Effects CC 2015.3
   * AviSynth+ および 以下のプラグイン　[独自カスタマイズ版インストーラーパッケージ](https://github.com/opap-jp/AviSynthPlus/releases/tag/Test-r1827)
     * MP_Pipeline
     * L-SMASH-Works (LSMASHSource)
   * MSYS2 および GNUMake
   * Git for windows 2.0以降
3. 以下のフォントがインストールされていること
    * [ラノベPOP](http://www.fontna.com/blog/1706/)
    * [MigU Fonts](http://mix-mplus-ipa.osdn.jp/migu/)
    * [MigMix Fonts](http://mix-mplus-ipa.osdn.jp/migmix/)
    * [M+ Fonts](https://mplus-fonts.osdn.jp/)
    * [Noto Sans Japanese](https://www.google.com/get/noto/)
    
### レンダリング方法
1. 要求環境に示された環境を準備してください。
2. コマンドラインAACエンコーダfdkaacをコンパイルして、fdkaac.exeをutils/binに置いてください。（注：特許関連の都合上、バイナリを配布することができません）
3. MSYS2のターミナルとしてminttyを使用する場合はロケール設定をja_JP.UTF-8に設定してください。
4. MSYS2 bashを起動し、このディレクトリに移動してください。
5. 「make」コマンドを実行して下さい。これにより、各カットのレンダリングが順次実行されます。レンダリング結果はdoga/_output以下に出力されます。
6. 上記の後、WEB向け動画ファイルを作成するためには、「make release」コマンドを実行してください。_releaseフォルダにWEB向けのmp4ファイル「kosys.mp4」が出力されます。
7. 同様に、BD向け動画ファイルを作成するためには、「make release-bd」コマンドを実行してください。_releaseフォルダにBD向けのmp4ファイル「kosys_bd.mp4」が出力されます。


## ライセンス
* [CC-BY 2.1 JP](http://creativecommons.org/licenses/by/2.1/jp/)
* [CC-BY 4.0](http://creativecommons.org/licenses/by/4.0/)
* [OPAP-UP 1.0](http://opap.jp/wiki/Licenses/OPAP-UP/1.0/jp)

一部異なるライセンスのファイルも含まれています。詳細はLICENSE.txtをご覧ください。

## クレジット
Copyright (C) 2016 OPAP-JP contributors.

この作品は[CC-BY 4.0](http://creativecommons.org/licenses/by/4.0/)および[CC-BY 2.1 JP](http://creativecommons.org/licenses/by/2.1/jp/)のもとに提供されています。
この作品をご利用の際には、適切なクレジットの表示が必要です。クレジットや著作権についての詳細は [https://opap.jp/wiki/redirect/kosys_ep02_authors](https://opap.jp/wiki/redirect/kosys_ep02_authors) をご覧ください。
