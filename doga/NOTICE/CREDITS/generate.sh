#!/bin/bash

export LANG=ja_JP.UTF-8

pushd "$(dirname $0)" > /dev/null
SCRIPT_DIR="$(pwd)"
popd > /dev/null

OUT_FILE="${1}"



echo -n "" >"${OUT_FILE}"

cat << EOM >>"${OUT_FILE}"
<!DOCTYPE html>
<html>
<head>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="utf-8" />
    <style>
        * {
            box-sizing: border-box;
            word-break: break-all;
        }

        html, body {
            margin: 0;
            padding: 0;
            background-color: #000000;
            color: #FFFFFF;
            font-family: 'Migu 1C';
            line-height: 1.5;
        }

        .wrapper {
            width: 1920px;
            max-width: 1920px;
            padding-top: 0;
            padding-left: 250px;
            padding-right: 250px;
        }

        section {
            width: auto;
            margin-bottom: 100px;
        }

        .section-head {
            font-size: 30px;
            padding: 0 10px;
            margin: 10px 0;
            border-left: solid 10px #FFFFFF;
            text-align: left;
        }

        .section-copyright {
            font-size: 50px;
            font-weight: bold;
            margin-bottom: 50px;
        }

        .section-contributors {
            font-size: 25px;
        }

        .multicol {
            column-count: 2;
            margin: 0 0 10px 0;
        }

        .contributors-list {
            margin: 0;
            padding-top: 0;
        }

            .contributors-list > li {
                /*display: block;*/
                margin: 0;
                padding: 0;
                break-inside: avoid;
            }

        .additional-info {
            display: block;
            padding-left: 10px;
            font-size: 90%;
            color: #AAAAAA;
        }

        .section-license {
            font-size: 25px;
        }
        
        .logo-cc-by {
            float: left;
            margin-right: 20px;
            height: 70px;
        }
        
        .license-list td {
            padding-right: 20px;
        }
        
        .for-low-resolution {
            font-size: 70px;
        }
        
        .note {
            font-size: 25px;
            margin: 10px 0;
            line-height: 1.5;
        }

        .translation {
            font-size: 90%;
            color: #AAAAAA;
        }
        
        
        
    </style>
</head>
<body>
    <div class="wrapper">
        <section class="section-copyright">
            Copyright &copy; 2016 OPAP-JP contributors.
        </section>
        <section class="section-contributors">
            <h1 class="section-head">クレジット表示 <span class="translation">(Attribution of Credit)</span> </h1>
            <div class="multicol">
                <ul class="contributors-list">
EOM

IFS=$'\n'
names=($( \
    cat "${SCRIPT_DIR}/../../../AUTHORS.txt" \
        | perl -pe 's/^\xEF\xBB\xBF//g; s/\r//; s/\t.*$//g' \
        | sed -E ' /^#/d' \
        | awk '!x[$0]++' \
        | perl -pe 's/\((.*)\)/<span class="additional-info">\1<\/span>/g'
))

for name in ${names[@]}; do
    echo "<li>${name}</li>"  >>"${OUT_FILE}"
done

cat << EOM >>"${OUT_FILE}"
                </ul>
            </div>
            <div class="note">
                この作品は、上記の皆様方による作品・素材・資料類を翻案して制作されました。<br />
                <span class="translation">(This work is a adaptation of materials created by authors listed in the above. )</span><br />
            </div>
            <div class="note">
                上記の一覧は本作品への支持や賛同を示すものではありません。<br />
                <span class="translation">(The above list does not indicate any endorsements for this work.)</span>
            </div>
            <div class="note">
            </div>
        </section>
        <section class="section-license">
            <h1 class="section-head">ライセンス <span class="translation">(License)</span></h1>
            <img class="logo-cc-by" src="by.svg" />
            <table class="license-list">
                <tr><td>CC-BY 4.0</td><td>http://creativecommons.org/licenses/by/4.0/</td></tr>
                <tr><td>CC-BY 2.1 JP</td><td>http://creativecommons.org/licenses/by/2.1/jp/ </td></tr>
            </table>
        </section>
    </div>
EOM
cp by.svg "$(dirname "${OUT_FILE}")"

"${SCRIPT_DIR}/../../../utils/bin/IECapt" --min-width=1920 --silent --max-wait=10000 --url="file:///$(cygpath -w -a "${OUT_FILE}" | sed 's/\\/\//g')" --out="${OUT_FILE}.png"

