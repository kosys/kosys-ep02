################################################################################
# This file is contributors list of this directory and its sub directories. 
# See <repository root>/common/AUHTORS.txt for contributors of common materials.
#-------------------------------------------------------------------------------
# このファイルはこのディレクトリおよびサブディレクトリの貢献者リストです。
# 共通資料の貢献者は<リポジトリルート>/common/AUTHORS.txtをご覧ください。 
################################################################################
#
# Note: 
#   - Some parts of this file are auto-generated. 
#   - This file contains Unicode characters and is encoded in UTF-8.
#   - Acknowledgements are not included.
#   - Authors of external material(s) may not be included.
#     Their names may be available at its file(s) or document(s).
#   - Auto-generated part of this contributors list is sorted by first
#     date in ascending order. If contribution date is unknown, their 
#     names are listed in the last. 
#   - Revision history is available at 
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep02 . 
#
# 注記: 
#   - このファイルの一部は自動生成されたものです。
#   - このファイルにはUnicode文字が含まれており、UTF-8でエンコードされています。
#   - 謝辞は含まれていません。
#   - 外部の素材や資料の著者情報は含まれていないことがあります。それらの著者情報
#     は当該の素材や資料のファイルやドキュメントに記載されていることがあります。
#   - この貢献者リストの自動生成部分は、最初の貢献日の昇順に並べられています。
#     もし、貢献日が不明の場合はリストの最後に記載されています。
#   - 更新履歴は以下からご覧いただけます。 
#     https://git.opap.jp/projects/KOSYS/repos/kosys-ep02  
#

Butameron
Stars/Dover
玉虫型偵察器
### END AUTO-GENERATED LIST.  LEAVE THIS LINE HERE; SCRIPTS LOOK FOR IT. ###
Butameron	背景作画
Stars/Dover	キャラ作画
Butameron	コンテ,キャラクター原案
なめたけ	キャラクターデザイン(夢前さくら),キャラクターデザイン(葛城総務部長)
絢嶺るり	キャラクター原案